<?php
/*
Plugin Name: Random Unambigious ShortURLs
Plugin URI: https://gitlab.com/walterebert/yourls-random-unambigious-shorturls
Description: Assign random unambigious keywords to shorturls, like bitly (sho.rt/hJudjK). Based on Random ShortURLs by Ozh.
Version: 1.0
Author: Walter Ebert
Author URI: https://walterebert.com/
*/

/* Release History:
*
* 1.0 Initial release
*/

// No direct call
if( !defined( 'YOURLS_ABSPATH' ) ) die();

// Only register things if the old third-party plugin is not present
if( function_exists('ozh_random_keyword') ) {
    yourls_add_notice( "<b>Random ShortURLs</b> plugin cannot function unless <b>Random Keywords</b> is removed first." );
} else {
    // filter registration happens conditionally, to avoid conflicts
    // settings action is left out here, as it allows checking settings before deleting the old plugin
    yourls_add_filter( 'random_keyword', 'wee_random_unambigious_shorturl' );
    yourls_add_filter( 'get_next_decimal', 'wee_random_unambigious_shorturl_next_decimal' );
}

// Generate a random keyword
function wee_random_unambigious_shorturl() {
    if ( defined( 'YOURLS_URL_CONVERT' ) && in_array( YOURLS_URL_CONVERT, [ 54, 62, 64 ] ) ) {
        // Mixed case
        $charset = '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456787';
    }
    else {
        // Lower case
        $charset = '2345678abcdefhijkmnpqrstuvwxyz234578';
    }

    $possible = yourls_apply_filter( 'get_shorturl_charset', $charset );

    $str='';
    while( strlen( $str ) < yourls_get_option( 'wee_random_unambigious_shorturls_length', 8 ) ) {
        $str .= substr($possible, wee_random_unambigious_shorturl_rand( 0, strlen( $possible ) - 1 ), 1 );
    }
    return $str;
}

// Don't increment sequential keyword tracker
function wee_random_unambigious_shorturl_next_decimal( $next ) {
    return ( $next - 1 );
}

// Plugin settings page etc.
yourls_add_action( 'plugins_loaded', 'wee_random_unambigious_shorturl_add_settings' );
function wee_random_unambigious_shorturl_add_settings() {
    yourls_register_plugin_page( 'wee_random_unambigious_shorturl_settings', 'Random Unambigious ShortURLs Settings', 'wee_random_unambigious_shorturl_settings_page' );
}

function wee_random_unambigious_shorturl_rand($min, $max) {
    $min = (int) $min;
    $max = (int) $max;

    if (function_exists('mt_rand')) {
        return mt_rand($min, $max);
    }

    return rand($min, $max);
}

function wee_random_unambigious_shorturl_settings_page() {
    // Check if form was submitted
    if( isset( $_POST['random_length'] ) ) {
        // If so, verify nonce
        yourls_verify_nonce( 'wee_random_unambigious_shorturl_settings' );
        // and process submission if nonce is valid
        wee_random_unambigious_shorturl_settings_update();
    }

    $random_length = yourls_get_option('wee_random_unambigious_shorturls_length', 8);
    $nonce = yourls_create_nonce( 'wee_random_unambigious_shorturl_settings' );

    echo <<<HTML
        <main>
            <h2>Random Unambigious ShortURLs Settings</h2>
            <form method="post">
            <input type="hidden" name="nonce" value="$nonce" />
            <p>
                <label>Random Keyword Length</label>
                <input type="number" name="random_length" min="1" max="128" value="$random_length" />
            </p>
            <p><input type="submit" value="Save" class="button" /></p>
            </form>
        </main>
HTML;
}

function wee_random_unambigious_shorturl_settings_update() {
    $random_length = 0;
    if (
        isset($_POST['random_length']) &&
        preg_match('/^[0-9]+$/', $_POST['random_length'])
    ) {
        $random_length = $_POST['random_length'];
    }

    if( $random_length > 0 ) {
        yourls_update_option( 'wee_random_unambigious_shorturls_length', intval( $random_length ) );
    } else {
        echo "Error: No valid length value given.";
    }
}
