# YOURLS Random Unambigious ShortURLs

[YOURLS](https://yourls.org/) plugin to assign random unambigious keywords to shorturls, like bitly (sho.rt/hJudjK). Based on Random ShortURLs by Ozh.

# License
[MIT](https://spdx.org/licenses/MIT.html)
